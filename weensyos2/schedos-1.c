#include "schedos-app.h"
#include "x86sync.h"

/*****************************************************************************
 * schedos-1
 *
 *   This tiny application prints red "1"s to the console.
 *   It yields the CPU to the kernel after each "1" using the sys_yield()
 *   system call.  This lets the kernel (schedos-kern.c) pick another
 *   application to run, if it wants.
 *
 *   The other schedos-* processes simply #include this file after defining
 *   PRINTCHAR appropriately.
 *
 *****************************************************************************/

#ifndef PRINTCHAR
#define PRINTCHAR	('1' | 0x0C00)
#endif

#ifndef PRIORITY
#define PRIORITY 1
#endif

#define EXTRA_CREDIT

void
start(void)
{
	int i;

	sys_user1(PRIORITY);
	sys_getticket();
	for (i = 0; i < RUNCOUNT; i++) {
		// Write characters to the console, yielding after each one.

	//using system call
#ifndef EXTRA_CREDIT
		sys_user2(PRINTCHAR);
#endif

	//using atomic operations (extra credit)
#ifdef EXTRA_CREDIT
		while (atomic_swap(&lock, 1) != 0)
			continue;
		*cursorpos++ = PRINTCHAR;
		atomic_swap(&lock, 0);
#endif

		sys_yield();
	}

	// Yield forever.
//	while (1)
//		sys_yield();
        sys_exit(0);
}
