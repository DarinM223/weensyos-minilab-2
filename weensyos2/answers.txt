Name: Darin Minamoto
UCLA ID: 704-140-102


-------------------------------------------------------------------------------

Response to Exercise 1:

The name of the scheduling algorithm is: Round Robin


(Exercise 2 is a coding exercise)
scheduling_algorithm = 1

Response to Exercise 3:

Average turnaround time for scheduling_algorithm 0: 1278.5 ms
Average turnaround time for scheduling_algorithm 1: 800 ms

Average wait time for scheduling_algorithm 0: 1.5 ms
Average wait time for scheduling_algorithm 1: 480 ms


Exercise 4:

Did you complete Exercise 4A or Exercise 4B? 4A

scheduling_algorithm = 2
The system call to set the priority is sys_user1. The priority for each process is defined in schedos-1.c, schedos-2.c, schedos-3.c, and schedos-4.c.
Right now the priorities are set like this:
Process 1: 1
Process 2: 1
Process 3: 3
Process 4: 4

Exercise 5: Process 4 because one time 4 doesn't get printed out which means that the interrupt happened before 4
otherwise it would be printed out.

(Exercise 6 is a coding exercise)
My synchronization mechanism uses a system call that prints a character. My system call to do that is sys_user2.


Anything else you'd like us to know:

Extra Credit 7:
Implemented lottery scheduling using the Random generator code from Steve Park & Dave Geyer. (scheduling_algorithm is 3). The Random function is dependant on the system time, which unfortunately doesn't work in Bochs,
so in that emulator, running it multiple times will yield the same result. However if it is run in qemu, which supports the system time, it will yield different
results, which is what is expected.

System Calls:
sys_getticket()
sys_deleteticket()
sys_gettime()

Extra Credit 8:
Implemented two synchronization mechanisms for printing characters:
1) Implements a new system call that prints a character (sys_user2)
2) Uses the atomic operations in x86sync.h directly

I used the preprocessor symbol EXTRA_CREDIT to determine whether the normal mechanism or the extra credit mechanism is used. EXTRA_CREDIT is defined by default, so
to test the normal mechanism you should uncomment the #define EXTRA_CREDIT line in schedos-1.c.


Note: Right now the default settings are this:
Extra credit synchronization mechanism: on
scheduling_algorithm = 0
interrupt_controller_init = 0
HZ = 2000
